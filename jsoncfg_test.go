// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package jsoncfg

import (
	"testing"
)

type TestConfig struct {
	TestString  string
	TestValue   int
	TestDefault string
}

func TestReadFile(t *testing.T) {
	cfg := &TestConfig{
		TestString:  "TestString",
		TestValue:   1234,
		TestDefault: "TestDefault",
	}

	if err := ReadFile(cfg, "jsoncfg_test.conf.json"); err != nil {
		t.Errorf(err.Error())
	}

	if expect := "Teststring from configfile"; cfg.TestString != expect {
		t.Errorf("Expected %q; got %q", expect, cfg.TestString)
	}

	if expect := 42; cfg.TestValue != expect {
		t.Errorf("Expected %d; got %d", expect, cfg.TestValue)
	}

	if expect := "TestDefault"; cfg.TestDefault != expect {
		t.Errorf("Expected %q; got %q", expect, cfg.TestDefault)
	}
}

func TestReadInvalidFile(t *testing.T) {
	cfg := &TestConfig{}

	if err := ReadFile(cfg, "jsoncfg_test.invalid-conf.json"); err == nil {
		t.Errorf("Reading invalid file did not fail")
	}

	if err := ReadFile(cfg, "jsoncfg_test.non-existing-conf.json"); err == nil {
		t.Errorf("Reading non-existing file did not fail")
	}
}
