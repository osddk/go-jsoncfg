// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package jsoncfg

import (
	"encoding/json"
	"io"
	"os"
)

// ReadFile reads and parses a json formatted file into a struct.
func ReadFile(c interface{}, fname string) (err error) {
	var file *os.File

	if file, err = os.Open(fname); err != nil {
		return err
	}

	if err = readInto(c, file); err != nil {
		return err
	}

	if err = file.Close(); err != nil {
		return err
	}

	return nil
}

// readInto parses data from a Reader as json and stores the result in a struct.
func readInto(c interface{}, reader io.Reader) (err error) {
	dec := json.NewDecoder(reader)
	for {
		if err = dec.Decode(&c); err == io.EOF {
			break
		} else if err != nil {
			return err
		}
	}
	return nil
}
