go-jsoncfg
==========

Introduction
------------

This is meant to be a simple JSON-encoded config file parser with default values support.

[![Build Status](https://drone.io/bitbucket.org/osddk/go-jsoncfg/status.png)](https://drone.io/bitbucket.org/osddk/go-jsoncfg/latest)


Installation
------------

Install the package to your $GOPATH by using the go tool:

    $ go get bitbucket.org/osddk/go-jsoncfg


Example
-----

Example of using go-jsoncfg:

```
import "bitbucket.org/osddk/go-jsoncfg"
import "log"

type MyConfig struct {
    SomeString      string
    SomeOtherString string
}

func main() {
    cfg := &MyConfig{
        SomeString:      "My Default String",
        SomeOtherString: "My Other Default String",
    }

    if err := jsoncfg.ReadFile(cfg, "config.json"); err != nil {
        log.Fatalln("Error reading config file:", err)
    }

    log.Println("SomeString:", cfg.SomeString)
    log.Println("SomeOtherString:", cfg.SomeOtherString)
}
```

If the config.json file contains:

    {"somestring": "My Configfile String"}

The above Go-code would result in:

    SomeString: My Configfile String
    SomeOtherString: My Other Default String
